import 'dart:convert';
//model that defines data about the user, as well as handling local storage of user data

import 'package:cloud_firestore/cloud_firestore.dart';

// To parse this JSON data, do
//
//     final user = userFromJson(jsonString);

//make local data from JSON file
User userFromJson(String str) {
  final jsonData = json.decode(str);
  return User.fromJson(jsonData);
}

//convert localuserData to JSON
String userToJson(User data) {
  final dyn = data.toJson();
  return json.encode(dyn);
}

// defines a user class
class User {
  // member variables
  String userId;
  String firstName;
  String lastName;
  String email;
  String match1;
  String match2;
  String match3;
  String match4;
  String match5;

  // constructor
  User({
    this.userId,
    this.firstName,
    this.lastName,
    this.email,
    this.match1,
    this.match2,
    this.match3,
    this.match4,
    this.match5,
  });

  // Deserializes the user variables from the json
  factory User.fromJson(Map<String, dynamic> json) => new User(
        userId: json["userId"],
        firstName: json["firstName"],
        lastName: json["lastName"],
        email: json["email"],
        match1: json["match1"],
        match2: json["match2"],
        match3: json["match3"],
        match4: json["match4"],
        match5: json["match5"],
      );

  // writes to json file
  Map<String, dynamic> toJson() => {
        "userId": userId,
        "firstName": firstName,
        "lastName": lastName,
        "email": email,
        "match1": match1,
        "match2": match2,
        "match3": match3,
        "match4": match4,
        "match5": match5,
      };

  factory User.fromDocument(DocumentSnapshot doc) {
    return User.fromJson(doc.data);
  }
}
