//model that defines a state for firebase login sequence

import 'package:firebase_auth/firebase_auth.dart';
import 'package:envizion/models/user.dart';
import 'package:envizion/models/settings.dart';

//defines local state of firebase data
class StateModel {
  bool isLoading;
  FirebaseUser firebaseUserAuth;
  User user;
  Settings settings;

  StateModel({
    this.isLoading = false,
    this.firebaseUserAuth,
    this.user,
    this.settings,
  });
}
