///entry point of the program, takes context of user login and decides what page to load as "homepage"

import 'package:envizion/ui/screens/matchup_page.dart';
import 'package:envizion/ui/screens/standings_page.dart';
import 'package:flutter/material.dart';
import 'package:envizion/util/state_widget.dart';
import 'package:envizion/ui/theme.dart';
import 'package:envizion/ui/screens/home.dart';
import 'package:envizion/ui/screens/sign_in.dart';
import 'package:envizion/ui/screens/sign_up.dart';
import 'package:envizion/ui/screens/forgot_password.dart';
import 'package:envizion/ui/screens/schedule_page.dart';

//base app class
class MyApp extends StatelessWidget {
  MyApp() {
    //Navigation.initPaths();
  }
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Envizion',
      theme: buildTheme(),
      //onGenerateRoute: Navigation.router.generator,
      debugShowCheckedModeBanner: false,
      // declare routes for pages for navigation
      // this will be used for the drawers
      routes: {
        '/': (context) => HomeScreen(), //load if already logged in
        '/signin': (context) => SignInScreen(), //load if not logged in
        '/signup': (context) => SignUpScreen(), //option fo sign up
        '/forgot-password': (context) => ForgotPasswordScreen(),  //option for forgot password
        //this section allows for app drawer otions to be avalible
        SchedulePage.tag: (context) => SchedulePage(),
        StandingsPage.tag: (context) => StandingsPage(),
        MatchUpPage.tag: (context) => MatchUpPage(),
      },
    );
  }
}

//entry point of application
void main() {
  StateWidget stateWidget = new StateWidget(
    child: new MyApp(),
  );
  runApp(stateWidget);
}
