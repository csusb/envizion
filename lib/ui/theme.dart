///the overall colortheme of the application

import 'package:flutter/material.dart';

//function that builds the theme for the app
ThemeData buildTheme() {
  // We're going to define all of our font styles
  // in this method:
  TextTheme _buildTextTheme(TextTheme base) {
    return base.copyWith( //text font and colors for header
        headline: base.headline.copyWith(
          fontFamily: 'Merriweather',
          fontSize: 40.0,
          color: const Color(0xFF555555),
        ),
        title: base.title.copyWith( //text font and colors for title
          fontFamily: 'Merriweather',
          fontSize: 15.0,
          color: const Color(0xFF555555),
        ),
        caption: base.caption.copyWith( //text font and colors for caption text
          color: const Color(0xFF555555),
        ),
        body1: base.body1.copyWith(color: const Color(0xFF555555)));  //text font and colors for text
  }

  // We want to override a default light blue theme.
  final ThemeData base = ThemeData.light();

  // And apply changes on it:
  return base.copyWith(
    textTheme: _buildTextTheme(base.textTheme),
    primaryColor: const Color(0xFF34af24),
    accentColor: const Color(0xFF42d62f),
    textSelectionColor: const Color(0xFFCCCCCC),
    primaryColorLight: const Color(0xFFd3d3d3),
    iconTheme: IconThemeData(
      color: const Color(0xFFCCCCCC),
      size: 20.0,
    ),
    accentIconTheme: IconThemeData(
      color: Colors.white
    ),
    buttonColor: Colors.white,
    backgroundColor: Colors.white,
  );
}
