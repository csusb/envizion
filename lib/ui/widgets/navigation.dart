/*
 * File: navigation.dart
 * Desription: Widget class for navigating through pages. Uses InkWell
 * to create custom tiles in Drawer for nicer user interface.
*/

import 'package:envizion/models/state.dart';
import 'package:envizion/ui/theme.dart';
import 'package:envizion/util/state_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
// imports of nvzn packages
import 'package:envizion/ui/screens/home.dart';
import 'package:envizion/ui/screens/schedule_page.dart';
import 'package:envizion/ui/screens/standings_page.dart';

// create a class that implements a custom drawer
class MyDrawer extends StatelessWidget{

  @override
  Widget build(BuildContext context) {
    final StateModel appState = StateWidget.of(context).state;
    final firstName = appState?.user?.firstName ?? '';
    final lastName = appState?.user?.lastName ?? '';
    return Drawer(
      // drawer uses a linear list to show buttons
      child: ListView(
        children: <Widget>[
          DrawerHeader( // drawerheader to decorate drawer with a blue rectangle
            decoration: BoxDecoration(
              gradient:  LinearGradient(colors: <Color>[buildTheme().primaryColor,
              buildTheme().accentColor,
              ])
            ),
            child: Container(
              child: Column(
                // creates a rectangle behind user image inside the drawerheader
                children: <Widget>[
                  Material(
                    color: buildTheme().primaryColor,
                    elevation: 10.0,
                    child: Padding(padding: EdgeInsets.all(8.0),
                    child: Image.asset('assets/images/default.png', width: 80, height: 80,),),
                  ),
                  Padding(padding: EdgeInsets.all(8.0),
                  child: Text('$firstName $lastName', style: TextStyle(fontWeight: FontWeight.bold, color: Colors.black54),)),
                ],
              )
            ),
          ),
          // create MyTile instances for each page that requires navigating
          // parameters include an Icon image for the button, page of navigation
          // and passes a function to navigate to the required page.
          MyTile(Icons.person, 'Profile', ()=>{Navigator.push(context, MaterialPageRoute(builder: (context) => HomeScreen()))}),
          MyTile(Icons.event, 'Schedule', ()=>{Navigator.push(context, MaterialPageRoute(builder: (context) => SchedulePage()))}),
          MyTile(Icons.equalizer, 'Standings', ()=>{Navigator.push(context, MaterialPageRoute(builder: (context) => StandingsPage()))}),
        ],
      ),
    );
  }
}

// create a class of a custom tile for nicer presentation
class MyTile extends StatelessWidget{

  // member variables for the class
  final IconData icon;
  final String text;
  final Function onTap;
  // constructor
  MyTile(this.icon, this.text, this.onTap);

  @override
  Widget build(BuildContext context){
    return Padding(
      padding: const EdgeInsets.fromLTRB(8.0, 0, 8.0, 0),
      child: InkWell( // creates an InkWell for flooding the button with color
        splashColor: buildTheme().primaryColor, // button fills with blue when pressed
        onTap: onTap, // calls onTap function passed through parameter
        child: Container(
          height:60, // size of button
          child: Row( // alignment of button
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Icon(icon, color: buildTheme().primaryColor),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(text,style: TextStyle(
                      fontSize: 16.0, fontWeight: FontWeight.bold
                    ),),
                    ),
                ],
                ),
                Icon(Icons.arrow_right, color: buildTheme().primaryColor)
            ],
            ),
      ),
      ),
    );
  }
}




