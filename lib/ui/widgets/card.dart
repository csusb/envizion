/*
 * Description: Card widget that will hold the information for
 * NFL team. Changes color state based on selection to pick teams.
 *
 */

import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:envizion/util/state_widget.dart';
import 'package:envizion/util/auth.dart';

class MyCard extends StatefulWidget {
  MyCard(this.match, this.userId, this.rowInt, {Key key}) : super(key: key);
  String match;
  String userId;
  int rowInt;
  @override
  _CardState createState() {
    return _CardState(match, userId, rowInt);
  }
}

class _CardState extends State<MyCard> {
  _CardState(this.match, this.userId, this.rowInt);
  String match;
  String userId;
  int rowInt;

  bool team1Selected = false;
  bool team2Selected = false;

  //helper function for writing user info to file
  storeUserData() async {
    var user = StateWidget.of(context).state.user;
    await Auth.storeUserLocal(user);
  }

  @override
  Widget build(BuildContext context) {
    var db = Firestore.instance
        .collection('users')
        .document(StateWidget.of(context).state.user.userId);

    void teamHighlight(var matchSelection) {
      if (int.parse(matchSelection) == 1) team1Selected = true;
      if (int.parse(matchSelection) == 2) team2Selected = true;
    }

    var userState = StateWidget.of(context).state.user;

    //hacky as fuck, replace with a proper list later
    switch (rowInt) {
      case 0:
        {
          teamHighlight(userState.match1);
        }
        break;
      case 1:
        {
          teamHighlight(userState.match2);
        }
        break;
      case 2:
        {
          teamHighlight(userState.match3);
        }
        break;
      case 3:
        {
          teamHighlight(userState.match4);
        }
        break;
      case 4:
        {
          teamHighlight(userState.match5);
        }
        break;
    }

    return StreamBuilder<QuerySnapshot>(
      stream: Firestore.instance.collection('MatchUps').snapshots(),
      builder: (context, snapshot) {
        if (!snapshot.hasData) return LinearProgressIndicator();

        // return Text(snapshot.data.data['match3']);
        return Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            // first container will be for the first team on the matchup
            Container(
              //color: Colors.orange,
              height: 100,
              width: 150.0,
              child: Card(
                color: team1Selected
                    ? Theme.of(context).accentColor.withOpacity(0.6)
                    : Colors.white.withOpacity(0.8),
                child: FlatButton(
                  child: ListView(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.fromLTRB(0, 8.0, 0, 8.0),
                      ),
                      Text(
                        // "match.team1"
                        snapshot.data.documents[rowInt].data['Team1'],
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 16.0),
                        textAlign: TextAlign.center,
                      ),
                      Text(
                        snapshot.data.documents[rowInt].data['Team1 Division'],
                        textAlign: TextAlign.center,
                      ),
                    ],
                  ),
                  onPressed: (() {
                    setState(() {
                      db.updateData({match.toString(): '1'});
                      //more hacky schenenagins
                      switch (rowInt) {
                        case 0:
                          {
                            userState.match1 = "1";
                          }
                          break;
                        case 1:
                          {
                            userState.match2 = "1";
                          }
                          break;
                        case 2:
                          {
                            userState.match3 = "1";
                          }
                          break;
                        case 3:
                          {
                            userState.match4 = "1";
                          }
                          break;
                        case 4:
                          {
                            userState.match5 = "1";
                          }
                          break;
                      }
                      team1Selected = true;
                      team2Selected = false;
                      //write to file
                      storeUserData();
                    });
                  }),
                ),
              ),
            ),
            // Second container is for a button that will lead to the matchup stats for the teams
            Container(
              //color: Theme.of(context).primaryColorLight,
              height: 92.0,
              width: 50.0,
              child: RaisedButton(shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8.0)),
                child: Text(
                  'VS',
                  style: TextStyle(fontSize: 14.0,),
                ),
                onPressed: (() {
                  showModalBottomSheet(
                      context: context,
                      builder: (BuildContext context) {
                        return Container(
                          height: 100.0,
                          child: Text(
                              "Team 1 Wins: " +
                                  snapshot.data.documents[rowInt]
                                      .data['Team1 Win Record'] +
                                  "\nTeam 2 Wins: " +
                                  snapshot.data.documents[rowInt]
                                      .data['Team2 Win Record'] +
                                  "\n", 
                                  textAlign: TextAlign.center,
                              style: TextStyle(fontSize: 32.0,)),
                        );
                      });
                }),
              ),
            ),
            // second team of the matchup
            Container(
              //color: Colors.orange,
              height: 100,
              width: 150.0,
              child: Card(
                color: team2Selected
                    ? Theme.of(context).accentColor.withOpacity(0.6)
                    : Colors.white.withOpacity(0.8),
                child: FlatButton(
                  child: ListView(
                    children: <Widget>[
                      Padding(
                        padding: EdgeInsets.fromLTRB(0, 8.0, 0, 8.0),
                      ),
                      Text(
                        snapshot.data.documents[rowInt].data['Team2'],
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 16.0),
                        textAlign: TextAlign.center,
                      ),
                      Text(
                        snapshot.data.documents[rowInt].data['Team2 Division'],
                        textAlign: TextAlign.center,
                      ),
                    ],
                  ),
                  onPressed: (() {
                    setState(() {
                      db.updateData({match.toString(): '2'});
                      //even more hacky schenenagins
                      switch (rowInt) {
                        case 0:
                          {
                            userState.match1 = "2";
                          }
                          break;
                        case 1:
                          {
                            userState.match2 = "2";
                          }
                          break;
                        case 2:
                          {
                            userState.match3 = "2";
                          }
                          break;
                        case 3:
                          {
                            userState.match4 = "2";
                          }
                          break;
                        case 4:
                          {
                            userState.match5 = "2";
                          }
                          break;
                      }
                      team1Selected = false;
                      team2Selected = true;
                      //write to file
                      storeUserData();
                    });
                  }),
                ),
              ),
            ),
          ],
        );
      },
    );
  }
}
