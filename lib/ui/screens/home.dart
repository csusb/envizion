//home page of the application (for logged in users)

import 'package:envizion/ui/theme.dart';
import 'package:envizion/ui/widgets/navigation.dart';
import 'package:flutter/material.dart';
import 'package:envizion/models/state.dart';
import 'package:envizion/util/state_widget.dart';
import 'package:envizion/ui/screens/sign_in.dart';
import 'package:envizion/ui/widgets/loading.dart';

class HomeScreen extends StatefulWidget {
  // gives class a tag for identification
  static String tag = 'home-screen';
  _HomeScreenState createState() => _HomeScreenState();
}

//home screen page
class _HomeScreenState extends State<HomeScreen> {
  StateModel appState;
  bool _loadingVisible = false;
  @override
  void initState() {
    super.initState();
  }

  Widget build(BuildContext context) {
    //handles user sign in sequence
    appState = StateWidget.of(context).state;
    // check if user is already signed in
    if (!appState.isLoading &&
        (appState.firebaseUserAuth == null ||
            appState.user == null ||
            appState.settings == null)) {
      // return to sign in screen if not signed in
      return SignInScreen();
    } else {
      if (appState.isLoading) {
        _loadingVisible = true;
      } else {
        _loadingVisible = false;
      }

      // set the user image on the screen
      final logo = Hero(
        tag: 'hero',
        child: CircleAvatar(
            backgroundColor: Colors.transparent,
            radius: 60.0,
            child: ClipOval(
              child: Image.asset(
                'assets/images/default.png',
                fit: BoxFit.cover,
                width: 120.0,
                height: 120.0,
              ),
            )),
      );

      // defines a sign out button
      final signOutButton = Padding(
        padding: EdgeInsets.symmetric(vertical: 16.0),
        child: RaisedButton(
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(24),
          ),
          onPressed: () { // sign the user out if button is pressed
            StateWidget.of(context).logOutUser();
          },
          padding: EdgeInsets.all(12),
          color: Theme.of(context).primaryColor,
          child: Text('SIGN OUT', style: TextStyle(color: Colors.white)),
        ),
      );


      final scoreLabel = Align(
        alignment: Alignment.center,
        child: Text(
          'Score: 0-0',
          style: TextStyle(color: Colors.black87, fontWeight: FontWeight.bold),
        ),
      );

      // defines a button for signup
      /*final signInLabel = FlatButton(
        child: Text(
          'Sign In',
          style: TextStyle(color: Colors.black54),
        ),
        onPressed: () { //navigate to the sign in page
          Navigator.pushNamed(context, '/signin');
        },
      );*/

//check for null https://stackoverflow.com/questions/49775261/check-null-in-ternary-operation
      //final userId = appState?.firebaseUserAuth?.uid ?? '';
      final email = appState?.firebaseUserAuth?.email ?? '';
      final firstName = appState?.user?.firstName ?? '';
      final lastName = appState?.user?.lastName ?? '';
      //final settingsId = appState?.settings?.settingsId ?? '';
      //final userIdLabel = Text('App Id: ');
      //final emailLabel = Text('Email: ', style: TextStyle(fontWeight:  FontWeight.bold,));
      //final firstNameLabel = Text('First Name: ');
      //final lastNameLabel = Text('Last Name: ');
      //final nameLabel = Text('Name: ');
      //final settingsIdLabel = Text('SetttingsId: ');

      // structure of the page
      return Scaffold(
        appBar: AppBar(
          title: Text('Profile'),
        ),
        drawer: MyDrawer(),
        backgroundColor: Colors.white,
        body: LoadingScreen(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 48.0),
              child: Center(
                child: SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: <Widget>[
                      logo,
                      SizedBox(height: 26.0),
                      Text('$firstName $lastName', style: TextStyle(fontWeight:  FontWeight.bold, fontSize: 24.0), textAlign: TextAlign.center,),
                      //SizedBox(height: 24.0),
                      /*userIdLabel,
                      Text(userId,
                          style: TextStyle(fontWeight: FontWeight.bold)),
                          */
                      SizedBox(height: 8.0),
                      scoreLabel,
                      SizedBox(height: 22.0),
                      Text('Email: $email',
                          style: TextStyle(fontWeight: FontWeight.bold), textAlign: TextAlign.center,),
                      SizedBox(height: 8.0),
                      /*firstNameLabel,
                      Text(firstName,
                          style: TextStyle(fontWeight: FontWeight.bold)),
                      SizedBox(height: 8.0),
                      lastNameLabel,
                      Text(lastName,
                          style: TextStyle(fontWeight: FontWeight.bold)),
                          */
                      SizedBox(height: 8.0),
                      /*settingsIdLabel,
                      Text(settingsId,
                          style: TextStyle(fontWeight: FontWeight.bold)), */
                      SizedBox(height: 8.0),
                      signOutButton,
                      //signInLabel, /* removed for prototype 4. Not needed */
                      //signUpLabel,
                      //forgotLabel
                    ],
                  ),
                ),
              ),
            ),
            inAsyncCall: _loadingVisible),
      );
    }
  }
}
