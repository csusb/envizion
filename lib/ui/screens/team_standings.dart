import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:envizion/ui/theme.dart';
import 'package:envizion/ui/widgets/navigation.dart';
import 'package:flutter/material.dart';

//page that shows how the teams are performing

class TeamStandings extends StatefulWidget {
 @override
 _TeamStandingsState createState() {
   return _TeamStandingsState();
 }
}

class _TeamStandingsState extends State<TeamStandings> {
 @override
 Widget build(BuildContext context) {
   return Scaffold(
     appBar: AppBar(
        title: Text('Standings: Team Standings'),
        backgroundColor: Theme.of(context).primaryColor,
      ),
     drawer: MyDrawer(),
     body: _buildBody(context),
   );
 }

 Widget _buildBody(BuildContext context) {
   return StreamBuilder<QuerySnapshot>(
     stream: Firestore.instance.collection('NFL').snapshots(),
     builder: (context, snapshot) {
       if (!snapshot.hasData) return LinearProgressIndicator();

       return _buildList(context, snapshot.data.documents);
     },
   );
 }

 Widget _buildList(BuildContext context, List<DocumentSnapshot> snapshot) {
   return ListView(
     padding: const EdgeInsets.only(top: 20.0),
     children: snapshot.map((data) => _buildListItem(context, data)).toList(),
   );
 }

 Widget _buildListItem(BuildContext context, DocumentSnapshot data) {
   final record = Record.fromSnapshot(data);

   return Padding(
     key: ValueKey(record.teamName),
     padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 8.0),
     child: Container(
       decoration: BoxDecoration(
         border: Border.all(color: Colors.grey),
         borderRadius: BorderRadius.circular(5.0),
       ),
       child: ListTile(
         title: Text(record.teamName),
         trailing: Text(record.division),
        //  onTap: () => Firestore.instance.runTransaction((transaction) async {
        //        final freshSnapshot = await transaction.get(record.reference);
        //        final fresh = Record.fromSnapshot(freshSnapshot);

        //        await transaction
        //            .update(record.reference, {'votes': fresh.votes + 1});
        //      }),
       ),
     ),
   );
 }
}

class Record {
 final String teamName;
 final String division;
 final DocumentReference reference;

 Record.fromMap(Map<String, dynamic> map, {this.reference})
     : assert(map['Team'] != null),
       assert(map['Division'] != null),
       teamName = map['Team'],
       division = map['Division'];

 Record.fromSnapshot(DocumentSnapshot snapshot)
     : this.fromMap(snapshot.data, reference: snapshot.reference);

 @override
 String toString() => "Record<$teamName:$division>";
}