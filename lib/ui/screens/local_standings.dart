import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:envizion/ui/theme.dart';
import 'package:envizion/ui/widgets/navigation.dart';
import 'package:envizion/util/state_widget.dart';
import 'package:flutter/material.dart';

//file that lists the local standings of the user

class LocalStandings extends StatefulWidget {
  @override
  _LocalStandingsState createState() {
    return _LocalStandingsState();
  }
}

class _LocalStandingsState extends State<LocalStandings> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Standings: Local Standings'),
        backgroundColor: Theme.of(context).primaryColor,
      ),
      drawer: MyDrawer(),
      body: _buildBody(context),
    );
  }

  Widget _buildBody(BuildContext context) {
    return StreamBuilder<DocumentSnapshot>(
      stream: Firestore.instance
          .collection('users')
          .document(StateWidget.of(context).state.user.userId)
          .snapshots(),
      builder: (context, snapshot) {
        if (!snapshot.hasData) return LinearProgressIndicator();

        // return Text(snapshot.data.data['match3']);
        return Center(
          child: Text(
            'View your standing against friends',
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 28.0,
            ),
          ),
        );
      },
    );
  }
}
