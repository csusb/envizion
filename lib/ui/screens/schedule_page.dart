import 'package:envizion/ui/widgets/card.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:envizion/util/state_widget.dart';
///page for displaying game schedule and matchups

import 'package:envizion/ui/widgets/navigation.dart';
import 'package:flutter/material.dart';

class SchedulePage extends StatelessWidget{
  // gives class a tag for identification
  static String tag = 'schedule-page';

  // prompt to appear on the screen
  @override
  Widget build(BuildContext context){
    // structure of the import 'package:cloud_firestore/cloud_firestore.dart';page.
    return Scaffold(
      appBar: AppBar(
        title: Text('Schedule'),
      ),
      drawer: MyDrawer(),
      body: ListView(children: <Widget>[
        SizedBox(height: 6.0),
        MyCard('match1', StateWidget.of(context).state.user.userId,0),
        SizedBox(height: 6.0,),
        MyCard('match2', StateWidget.of(context).state.user.userId,1),
        SizedBox(height: 6.0,),
        MyCard('match3', StateWidget.of(context).state.user.userId,2),
        SizedBox(height: 6.0,),
        MyCard('match4', StateWidget.of(context).state.user.userId,3),
        SizedBox(height: 6.0,),
        MyCard('match5', StateWidget.of(context).state.user.userId,4),
        SizedBox(height: 6.0,),
      ],),
    );

  }
}