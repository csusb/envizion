import 'package:envizion/ui/theme.dart';
import 'package:envizion/ui/widgets/navigation.dart';
import 'package:flutter/material.dart';
import 'package:envizion/ui/screens/team_standings.dart';
import 'package:envizion/ui/screens/local_standings.dart';
import 'package:envizion/ui/screens/home.dart';
import 'package:envizion/ui/screens/schedule_page.dart';

class StandingsPage extends StatelessWidget {
  static String tag = 'standings-page';

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Flutter Bottom Nav Demo',
        //theme: ThemeData(
        //primarySwatch: Colors.lightBlue,
        //),
        //drawer: MyDrawer(),
        theme: buildTheme(),
        home: BottomNavExample());
    //home: FirstScreen());
  }
}

class BottomNavExample extends StatefulWidget {
  @override
  createState() => BottomNavExampleState();
}

class BottomNavExampleState extends State<BottomNavExample> {
  int index = 0;

  final List<Widget> _children = [
    //This list dictates which screen will be displayed ontap
    TeamStandings(), //replaced FirstScreen(),
    LocalStandings(), //replaced SecondScreen(),
    //ThirdScreen(),
    //FourthScreen(),
    //FifthScreen(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        // appBar: AppBar(
        //   title: Text('Schedule'),
        // ),
        // drawer: MyDrawer(),
        body: _children[index],
        bottomNavigationBar: TwoItemBottomNavBar(
            index: index,
            callback: (newIndex) => setState(
                  () => this.index = newIndex,
                )));

    /*appBar: AppBar(
        title: Text(
          'NVZN',
          style: TextStyle(
          fontStyle: FontStyle.italic,
          )
        ),
        backgroundColor: Colors.white,
      )
    );*/
  }
}

class Body extends StatelessWidget {
  Body(this.text);
  final String text;
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Center(
        child: Text(
          text,
          style: Theme.of(context).textTheme.display2,
        ),
      ),
    );
  }
}

class TwoItemBottomNavBar extends StatelessWidget {
  TwoItemBottomNavBar({this.index, this.callback});
  final int index;
  final Function(int) callback;

  @override
  Widget build(BuildContext context) {
    /// BottomNavigationBar is automatically set to type 'fixed'
    /// when there are three of less items
    return BottomNavigationBar(
      type: BottomNavigationBarType.shifting,
      showUnselectedLabels: true,
      selectedFontSize: 16,
      unselectedItemColor: Theme.of(context).textSelectionColor,
      currentIndex: index,
      onTap: callback,
      items: <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Icon(Icons.show_chart),
          title: Text('Team Standings'),
          backgroundColor: Theme.of(context).primaryColor,
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.home),
          title: Text('Local Leaderboard'),
          backgroundColor: Theme.of(context).accentColor,
        ),
      ],
    );
  }
}
