///page for showing user matchups

import 'package:envizion/ui/widgets/navigation.dart';
import 'package:flutter/material.dart';

class MatchUpPage extends StatelessWidget{
  // gives class a tag for identification
  static String tag = 'matchup-page';

  // prompt to appear on the screen
  @override
  Widget build(BuildContext context){
    final prompt = Padding(
      padding: EdgeInsets.all(8.0),
      child: Text(
        'MatchUp: Coming Soon',
        style: TextStyle(fontSize: 28.0, color: Colors.white),
      ),
    );

    // create a container the size of the screen and fills it with
    // a gradient of blue. Sets the prompt on the screen
    final body = Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.all(28.0),
      decoration: BoxDecoration(
        gradient: LinearGradient(colors: [
          Colors.lightBlue,
          Colors.lightBlueAccent,
        ]),
      ),
      child: Column(
        children: <Widget>[prompt],
      ),
    );

    // structure of the page
    return Scaffold(
      appBar: AppBar(
        title: Text('Matchup'),
      ),
      drawer: MyDrawer(),
      body: body,
    );

  }
}