///validates user input to prevent injection attacks
class Validator {
  // validate whether email entered is of correct pattern
  static String validateEmail(String value) {
    Pattern pattern = r'^[a-zA-Z0-9.]+@[a-zA-Z0-9]+\.[a-zA-Z]+';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value))
    // prompt user if email is not valid
      return 'Please enter a valid email address.';
    else
      return null;
  }

  // validate whether password entered is of correct pattern
  static String validatePassword(String value) {
    Pattern pattern = r'^.{6,}$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value))
    // prompt user for the correct format
      return 'Password must be at least 6 characters.';
    else
      return null;
  }

  // validate whether name entered is of correct pattern
  static String validateName(String value) {
    Pattern pattern = r"^[a-zA-Z]+(([',. -][a-zA-Z ])?[a-zA-Z]*)*$";
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value))
    // prompt user if name is not of correct format
      return 'Please enter a name.';
    else
      return null;
  }

  // validate whether number entered is of correct pattern
  static String validateNumber(String value) {
    Pattern pattern = r'^\D?(\d{3})\D?\D?(\d{3})\D?(\d{4})$';
    RegExp regex = new RegExp(pattern);
    if (!regex.hasMatch(value))
    // prompt if number is not correct format
      return 'Please enter a number.';
    else
      return null;
  }
}
